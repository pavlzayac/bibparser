package formatters;

import interfaces.IArticleFormatter;
import publications.Article;

public class ArticleFormatter implements IArticleFormatter {
    @Override
    public String format(Article article) {
        StringBuilder citationBuilder = new StringBuilder();

        citationBuilder.append(article.getId());
        citationBuilder.append('\n');

        if (article.getAuthor() != null) {
            citationBuilder.append(article.getAuthor());
            citationBuilder.append('\n');
        }

        if (article.getDoi() != null) {
            citationBuilder.append(article.getDoi());
            citationBuilder.append('\n');
        }

        if (article.getTitle() != null) {
            citationBuilder.append(article.getTitle());
            citationBuilder.append('\n');
        }

        if (article.getMonth() != null) {
            citationBuilder.append(article.getMonth());
            citationBuilder.append('\n');
        }

        if (article.getPages() != null) {
            citationBuilder.append(article.getPages());
            citationBuilder.append('\n');
        }

        if (article.getYear() != null) {
            citationBuilder.append(article.getYear());
            citationBuilder.append('\n');
        }

        if (article.getJournal() != null) {
            citationBuilder.append(article.getJournal());
            citationBuilder.append('\n');
        }

        if (article.getIssn() != null) {
            citationBuilder.append(article.getIssn());
            citationBuilder.append('\n');
        }

        if (article.getIssue() != null) {
            citationBuilder.append(article.getIssue());
            citationBuilder.append('\n');
        }

        if (article.getUrl() != null) {
            citationBuilder.append(article.getUrl());
            citationBuilder.append('\n');
        }

        return citationBuilder.toString();
    }
}
