package formatters;

import interfaces.IBookFormatter;
import publications.Book;

public class BookFormatter implements IBookFormatter {
    @Override
    public String format(Book book) {
        StringBuilder citationBuilder = new StringBuilder();

        citationBuilder.append(book.getId());
        citationBuilder.append('\n');

        if (book.getAuthor() != null) {
            citationBuilder.append(book.getAuthor());
            citationBuilder.append('\n');
        }

        if (book.getDoi() != null) {
            citationBuilder.append(book.getDoi());
            citationBuilder.append('\n');
        }

        if (book.getTitle() != null) {
            citationBuilder.append(book.getTitle());
            citationBuilder.append('\n');
        }

        if (book.getIsbn() != null) {
            citationBuilder.append(book.getIsbn());
            citationBuilder.append('\n');
        }

        if (book.getPublisher() != null) {
            citationBuilder.append(book.getPublisher());
            citationBuilder.append('\n');
        }

        if (book.getVolume() != null) {
            citationBuilder.append(book.getVolume());
            citationBuilder.append('\n');
        }

        if (book.getUrl() != null) {
            citationBuilder.append(book.getUrl());
            citationBuilder.append('\n');
        }

        return citationBuilder.toString();
    }
}
