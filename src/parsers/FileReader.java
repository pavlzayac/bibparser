package parsers;

import interfaces.IReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.Scanner;

public class BibFileReader implements IReader {
    private String path;

    public BibFileReader(String path) {
        this.path = path;
    }

    @Override
    public List<String> getLines() {
        try {
            File openFile = new File(path);
            FileReader bufferedReader = new FileReader(openFile);
            Scanner sc = new Scanner(bufferedReader);
        } catch (FileNotFoundException e) {

        }


        return List.of();
    }
}
