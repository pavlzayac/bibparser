package interfaces;

import java.util.List;
public interface IReader {
    List<String> getLines();
}
