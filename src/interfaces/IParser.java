package interfaces;

import publications.Publication;

import java.util.List;

/*
Most important thing:
Using the Map<K, V>, probably Map<String, String>

@article{id
doi = {somedoi}

doi goes into Key, somedoi into Value

doing split over "=", trim both halves

handling {somedoi} with split("[{}]+"), square braces are a set of characters, meaning "split over all opening and curly braces"
Warning: do mind that you may get empty Strings, "{somedoi}".split("[{}]+") can give more than 1 String

Then
new Article(map.get("author"), Integer.valueOf(map.get("month")), ...
 */

public interface IParser {
    List<Publication> parse(IReader reader);
}
