package interfaces;

import publications.Book;

public interface IBookFormatter {
    String format(Book book);
}
