package publications;

import interfaces.ICitable;

public abstract class Publication implements ICitable {
    private final String id;
    private String author;
    private String doi;
    private String title;
    private String url;

    public Publication(String id, String author, String doi, String title, String url) {
        this.id = id;
        this.author = author;
        this.doi = doi;
        this.title = title;
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public String getDoi() {
        return doi;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }
}
